package com.example.myapplication

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.Button
import android.widget.Toast
import java.util.*
import kotlin.concurrent.schedule

class MainActivity() : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var time1 = 0
        var time2 = 0
        val buttonPlr1 = findViewById<Button>(R.id.buttonPlr1)
        val buttonPlr2 = findViewById<Button>(R.id.buttonPlr2)
        val buttonPlay = findViewById<Button>(R.id.buttonPlay)

        buttonPlr1.setTextColor(Color.BLACK)
        buttonPlr1.setBackgroundColor(Color.YELLOW)
        buttonPlr1.setTextSize(1,64f)
        buttonPlr1.text = time1.toString()

        buttonPlr2.setTextColor(Color.BLACK)
        buttonPlr2.setBackgroundColor(Color.YELLOW)
        buttonPlr2.setTextSize(1,64f)
        buttonPlr2.setText(time2.toString())

        buttonPlay.setOnClickListener {
            time1 = 300
            time2 = 300
            buttonPlr1.setText(time1.toString())
            buttonPlr2.setText(time2.toString())
        }

        val timer1 = object : CountDownTimer(300000,1000) {
            override fun onTick(p0: Long) {
                buttonPlr1.setText((--time1).toString())
            }


            override fun onFinish() {
                cancel()
            }
        }

        val timer2 = object : CountDownTimer(300000,1000) {
            override fun onTick(p0: Long) {
                buttonPlr2.setText((--time2).toString())
            }

            override fun onFinish() {
                cancel()
            }
        }

        buttonPlr1.setOnClickListener {
            timer2.start()
            timer1.onFinish()
            buttonPlr1.setEnabled(false)
            buttonPlr2.setEnabled(true)
        }

        buttonPlr2.setOnClickListener {
            timer1.start()
            timer2.onFinish()
            buttonPlr2.setEnabled(false)
            buttonPlr1.setEnabled(true)
        }
    }
}